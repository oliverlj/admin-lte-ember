import Component from '@glimmer/component';

interface AlteCardArgs {
  collapsed?: boolean;
  loading: boolean;
  title?: string;
  titleIcon?: string;
  titleIconPrefix?: string;
  type?: string;
}

export default class AlteLteCard extends Component<AlteCardArgs> {
  get collapsable() {
    return this.args.collapsed != undefined;
  }
}
