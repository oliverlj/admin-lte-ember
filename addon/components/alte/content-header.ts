import Component from '@glimmer/component';

interface ContentHeaderComponentArgs {
  container: string;
  name?: string;
}

export default class ContentHeaderComponent extends Component<ContentHeaderComponentArgs> {
  get container() {
    return this.args.container ?? 'container-fluid';
  }
}
