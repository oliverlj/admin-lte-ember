import Component from '@glimmer/component';

interface AlteCardHeaderArgs {
  collapsed?: boolean;
  onCollapse: () => void;
  noBottomBorder: boolean;
  title: string;
  titleClasses: string;
  titleIcon: string;
  titleIconPrefix: string;
}

export default class AlteLteCardHeader extends Component<AlteCardHeaderArgs> {
  get noBottomBorder() {
    return this.args.collapsed || this.args.noBottomBorder;
  }
}
