import Component from '@glimmer/component';

interface AlteCardContentArgs {
  collapsable?: boolean;
  collapsed?: boolean;
}

export default class AlteCardContent extends Component<AlteCardContentArgs> {
  get collapsed() {
    return this.args.collapsed;
  }
}
