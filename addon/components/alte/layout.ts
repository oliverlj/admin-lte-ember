import Component from "@glimmer/component";

interface AlteLayoutComponentArgs {
  layout: string;
}

export default class AlteLayoutComponent extends Component<
  AlteLayoutComponentArgs
> {
  constructor(owner: any, args: AlteLayoutComponentArgs) {
    super(owner, args);
    if (args.layout) {
      document.body.classList.add(args.layout);
    }
  }

  willDestroy() {
    if (this.args.layout) {
      document.body.classList.remove(this.args.layout);
    }
  }
}
