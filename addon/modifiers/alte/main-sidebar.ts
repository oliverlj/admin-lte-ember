import { modifier } from 'ember-modifier';

const CLASS_MAIN_SIDEBAR = 'main-sidebar';
const CLASS_SIDEBAR_MINI = 'sidebar-mini';

export default modifier((/*element, params, hash*/) => {
  document.body.classList.add(CLASS_SIDEBAR_MINI);
  return () => {
    if (!document.querySelector(`.${CLASS_MAIN_SIDEBAR}`)) {
      document.body.classList.remove(CLASS_SIDEBAR_MINI);
    }
  };
});
