import { action } from '@ember/object';
import Modifier from 'ember-modifier';

interface AlteDarkModeModifierArgs {
  positional: [];
  named: {
    enableRemember?: boolean;
    onLightened?: () => void;
    onDarkened?: () => void;
  };
}

export default class AlteDarkModeModifier extends Modifier<AlteDarkModeModifierArgs> {
  private static readonly CLASS_DARK_MODE = 'dark-mode';
  private static readonly EVENT_KEY = '.lte.darkmode';

  private enableRemember: boolean = false;

  didInstall() {
    this.init();
    if (this.element) {
      this.element.addEventListener('click', this.onClick, true);
    }
  }

  didReceiveArguments() {
    if (this.args.named.enableRemember) {
      this.enableRemember = this.args.named.enableRemember;
    }
  }

  @action
  onClick(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.toggle();
  }

  remember() {
    if (this.enableRemember) {
      const darkMode = localStorage.getItem(`remember${AlteDarkModeModifier.EVENT_KEY}`);
      if (darkMode === 'true' && !document.body.classList.contains(AlteDarkModeModifier.CLASS_DARK_MODE)) {
        this.darken();
      } else if (darkMode == 'false') {
        this.lighten();
      }
    }
  }

  darken() {
    document.body.classList.add(AlteDarkModeModifier.CLASS_DARK_MODE);
    if (this.enableRemember) {
      localStorage.setItem(`remember${AlteDarkModeModifier.EVENT_KEY}`, 'true');
    }
    if (this.args.named.onDarkened) {
      this.args.named.onDarkened();
    }
  }

  lighten() {
    document.body.classList.remove(AlteDarkModeModifier.CLASS_DARK_MODE);
    if (this.enableRemember) {
      localStorage.setItem(`remember${AlteDarkModeModifier.EVENT_KEY}`, 'false');
    }
    if (this.args.named.onLightened) {
      this.args.named.onLightened();
    }
  }

  toggle() {
    if (document.body.classList.contains(AlteDarkModeModifier.CLASS_DARK_MODE)) {
      this.lighten();
    } else {
      this.darken();
    }
  }

  willRemove() {
    if (this.element) {
      this.element.removeEventListener('click', this.onClick, true);
    }
  }

  private init() {
    this.remember();
  }
}
