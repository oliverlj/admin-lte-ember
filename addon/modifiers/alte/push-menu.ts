import { action } from '@ember/object';
import { later } from '@ember/runloop';
import Modifier from 'ember-modifier';

interface AltePushMenuModifierArgs {
  positional: [];
  named: {
    autoCollapseSize?: number;
    enableRemember?: boolean;
    noTransitionAfterReload?: boolean;
    onCollapsed?: () => void;
    onShown?: () => void;
  };
}

export default class AltePushMenuModifier extends Modifier<AltePushMenuModifierArgs> {
  private static readonly CLASS_NAME_COLLAPSED = 'sidebar-collapse';
  private static readonly CLASS_NAME_OPEN = 'sidebar-open';
  private static readonly CLASS_NAME_WRAPPER = 'wrapper';
  private static readonly ID_OVERLAY = 'sidebar-overlay';
  private static readonly EVENT_KEY = '.lte.pushmenu';

  private autoCollapseSize: number = 992;
  private autoCollapseOnResizeBoundFunction: () => void;
  private collapseBoundFunction: () => void;
  private enableRemember: boolean = false;
  private overlayDiv?: HTMLElement;
  private noTransitionAfterReload: boolean = true;

  constructor(owner: any, args: any) {
    super(owner, args);
    this.autoCollapseOnResizeBoundFunction = this.autoCollapseOnResize.bind(this);
    this.collapseBoundFunction = this.collapse.bind(this);
  }

  autoCollapse() {
    if (this.autoCollapseSize && window.innerWidth <= this.autoCollapseSize) {
      if (!document.body.classList.contains(AltePushMenuModifier.CLASS_NAME_OPEN)) {
        this.collapse();
      }
    }
  }

  autoCollapseOnResize() {
    if (this.autoCollapseSize) {
      if (window.innerWidth <= this.autoCollapseSize) {
        if (!document.body.classList.contains(AltePushMenuModifier.CLASS_NAME_OPEN)) {
          this.collapse();
        }
      } else {
        if (document.body.classList.contains(AltePushMenuModifier.CLASS_NAME_OPEN)) {
          document.body.classList.remove(AltePushMenuModifier.CLASS_NAME_OPEN);
        }
      }
    }
  }

  collapse() {
    if (this.autoCollapseSize) {
      if (window.innerWidth <= this.autoCollapseSize) {
        document.body.classList.remove(AltePushMenuModifier.CLASS_NAME_OPEN);
      }
    }

    document.body.classList.add(AltePushMenuModifier.CLASS_NAME_COLLAPSED);

    if (this.enableRemember) {
      localStorage.setItem(`remember${AltePushMenuModifier.EVENT_KEY}`, AltePushMenuModifier.CLASS_NAME_COLLAPSED);
    }

    if (this.args.named.onCollapsed) {
      this.args.named.onCollapsed();
    }
  }

  didInstall() {
    this.init();
    if (this.element) {
      this.element.addEventListener('click', this.onClick, true);
    }
  }

  didReceiveArguments() {
    if (this.args.named.autoCollapseSize) {
      this.autoCollapseSize = this.args.named.autoCollapseSize;
    }
    if (this.args.named.enableRemember) {
      this.enableRemember = this.args.named.enableRemember;
    }
    if (this.args.named.noTransitionAfterReload) {
      this.noTransitionAfterReload = this.args.named.noTransitionAfterReload;
    }
  }

  expand() {
    if (this.autoCollapseSize) {
      if (window.innerWidth <= this.autoCollapseSize) {
        document.body.classList.add(AltePushMenuModifier.CLASS_NAME_OPEN);
      }
    }

    document.body.classList.remove(AltePushMenuModifier.CLASS_NAME_COLLAPSED);

    if (this.enableRemember) {
      localStorage.setItem(`remember${AltePushMenuModifier.EVENT_KEY}`, AltePushMenuModifier.CLASS_NAME_OPEN);
    }

    if (this.args.named.onShown) {
      this.args.named.onShown();
    }
  }

  @action
  onClick(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.toggle();
  }

  remember() {
    if (this.enableRemember) {
      const toggleState = localStorage.getItem(`remember${AltePushMenuModifier.EVENT_KEY}`);
      if (toggleState === AltePushMenuModifier.CLASS_NAME_COLLAPSED) {
        if (this.noTransitionAfterReload) {
          document.body.classList.add('hold-transition', AltePushMenuModifier.CLASS_NAME_COLLAPSED);
          later(() => {
            document.body.classList.remove('hold-transition');
          }, 50);
        } else {
          document.body.classList.add(AltePushMenuModifier.CLASS_NAME_COLLAPSED);
        }
      } else {
        if (this.noTransitionAfterReload) {
          document.body.classList.add('hold-transition');
          document.body.classList.remove('hold-transition', AltePushMenuModifier.CLASS_NAME_COLLAPSED);
          later(() => {
            document.body.classList.remove('hold-transition');
          }, 50);
        } else {
          document.body.classList.remove(AltePushMenuModifier.CLASS_NAME_COLLAPSED);
        }
      }
    }
  }

  toggle() {
    if (!document.body.classList.contains(AltePushMenuModifier.CLASS_NAME_COLLAPSED)) {
      this.collapse();
    } else {
      this.expand();
    }
  }

  willRemove() {
    if (this.element) {
      this.element.removeEventListener('click', this.onClick, true);
    }
    window.removeEventListener('resize', this.autoCollapseOnResizeBoundFunction);
    if (this.overlayDiv) {
      this.overlayDiv.removeEventListener('click', this.collapseBoundFunction);
      this.overlayDiv.remove();
    }
  }

  private createOverlayDiv() {
    const wrapper = document.getElementsByClassName(AltePushMenuModifier.CLASS_NAME_WRAPPER).item(0);

    if (wrapper) {
      let overlayDiv = document.getElementById(AltePushMenuModifier.ID_OVERLAY);
      if (!overlayDiv) {
        overlayDiv = document.createElement('div');
        overlayDiv.setAttribute('id', AltePushMenuModifier.ID_OVERLAY);
      }
      wrapper.append(overlayDiv);
      overlayDiv.addEventListener('click', this.collapseBoundFunction);

      this.overlayDiv = overlayDiv;
    }
  }

  private init() {
    this.createOverlayDiv();
    this.remember();
    this.autoCollapse();
    window.addEventListener('resize', this.autoCollapseOnResizeBoundFunction);
  }
}
