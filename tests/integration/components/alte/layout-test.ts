import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Component | alte/layout', (hooks) => {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<Alte::Layout />`);
    assert.dom('.wrapper').exists();

    await render(hbs`
      <Alte::Layout>
        template block text
      </Alte::Layout>
    `);
    assert.dom('.wrapper').exists();
    assert.equal(this.element.textContent?.trim(), 'template block text');
  });
});
