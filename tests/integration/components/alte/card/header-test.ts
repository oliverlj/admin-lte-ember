import { render } from '@ember/test-helpers';
import { setupRenderingTest } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import { module, test } from 'qunit';

module('Integration | Component | alte/card/card-header', (hooks) => {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`<Alte::Card::Header />`);

    assert.dom('.card-header').exists();
  });
});
