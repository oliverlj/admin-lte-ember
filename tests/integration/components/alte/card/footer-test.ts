import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Component | alte/card/card-footer', (hooks) => {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`<Alte::Card::Footer />`);

    assert.dom('.card-footer').exists();
  });
});
