import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('index1');
  this.route('index2');
  this.route('pages', function () {
    this.route('layout', function () {
      this.route('top-nav');
    });
    this.route('widgets');
  });
  this.route('index3');
});
