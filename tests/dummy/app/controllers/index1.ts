import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class Index1Controller extends Controller {
  @tracked
  directChatCollapsed = false;

  @action
  toggleDirectChatCollapse() {
    this.directChatCollapsed = !this.directChatCollapsed;
  }
}
