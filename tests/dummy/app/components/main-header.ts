import Component from '@glimmer/component';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MainHeader extends Component {
  @tracked
  type = 'light';

  @action
  darken() {
    this.type = 'dark';
  }

  @action
  lighten() {
    this.type = 'light';
  }
}
