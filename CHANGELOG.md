# CHANGELOG

<!--- next entry here -->

## 0.2.0
2021-10-31

### Features

- more flexible content header (6c9e407f5b1e369e063bc50a9996b6148285f9e1)

## 0.1.0
2021-09-16

### Features

- include adminlte library (a269f1243eb53d593d8bdb1f8ea642fc3491dccd)
- add layout component (9a0e30b223f82b9ec44c52f593ed35cf7fe60a4b)
- add layout components (494aa607fe84f21cac0e08831468ded356b468d0)
- add card (cc0d0fc6965c15b98bf6581d0652c0ea03ad5c41)
- add source sans pro font (5c6137e4760ba8c502fe67a289469a459785afdb)
- add expandable card (0e6209d3ba4d19b8930ed327e922928989258c44)
- add navbar (0ad9c1435e11eaff0265f9150beb5fbeb8322c91)
- add dark mode (73f4d5e8c331a5486b5a8539fef85a2a6b0a127b)

### Fixes

- add missing class on content header title (f82d7218c0182239cd6ed92fb28f96f8f10fb14a)
- adminlte css styles not imported (55cf641fbd8b6b535864bbeeb5cbcac22b1b97cb)
- remove css from addon (07361e8aa9657aed4f1a4f5026e6c7639ad6a210)
- move ember bootstrap to dependencies (daec078b835949440a698ce81671a82bd6af5c9f)
- **main-sidebar:** use a modifier (1b676e67dc0f58f148d28c090d7cb1e01addb2e5)
- linter (61a7c37b7f0a303c39d0c9a5161a432b6f2fc405)
- collapsable card (1e5c802d7c7812e8c78ed229fe105b3bdcf6bf32)